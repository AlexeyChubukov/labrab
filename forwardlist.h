#pragma once

struct Node
{
	Node* next;
	int data;
};

class ForwardList
{
	Node* first;
public:
	Node* getfirst();
	ForwardList();
	ForwardList(Node* first);
	~ForwardList();

	void insert(int value);
	bool remove(int value);
	bool remove_all(int value);
	void print();
};