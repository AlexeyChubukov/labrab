
#ifdef WIN32
#include <stdafx.h>
#endif
#include <iostream>
using namespace std;

#include "forwardlist.h"

Node* ForwardList::getfirst()
		{
			return first;
		}

ForwardList::ForwardList()
		{
			first = nullptr;
		}
ForwardList::ForwardList(Node* first)
		{
			this->first = first;
		}
ForwardList::~ForwardList()
{
	while (first != nullptr)
	{
		Node *node_copy = first->next;
		delete first;
		first = node_copy;
	}
}
	
void ForwardList::insert(int value)
		{
			Node *p = first;
			while (p->next!= nullptr)
			{
				p = p->next;
			}
			p->next = new Node{ nullptr,value };
		}

bool ForwardList::remove_all(int value)
{
	Node *p = first;
	int count = 0;
	while (p!=nullptr)
	{
		if (p->data == value)
		{
			count++;
		}
		p = p->next;
	}
	if (count)
	{
		for (int i = 0; i < count; i++)
		{
			ForwardList::remove(value);
		}
		return true;
	}
	else
	{
		return false;
	}
	
}

bool ForwardList::remove(int value)
{
	Node *p = first;
	if (p->data == value)
	{
		Node *temp = first;
		first = first->next;
		delete temp;
		return true;
	}
	else
	{
		while (p != nullptr)
		{
			if (p->next->data == value)
			{
				Node *p1 = p->next;
				p->next = p->next->next;
				delete p1;
				return true;
			}
			p = p->next;
		}
	}
	return false;
}

void ForwardList::print()
		{
			Node *p = first;
			while (p != nullptr)
			{
				cout << p->data << "|";
				p = p->next;
			}
		}

