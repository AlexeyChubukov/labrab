// lab2.cpp : Defines the entry point for the console application.
//
#ifdef WIN32
#include "stdafx.h"
#endif
#include <iostream>
#include "forwardlist.h"

using namespace std;


int main()
{
	const int N = 5;
	Node *node = new Node{ nullptr,1 };
	ForwardList list1(node);
	//Node *first = node;

	for (int i = 1; i < N; i++)
	{
		node->next = new Node{ nullptr, i + 1 };
		node = node->next;
	}
	cout << list1.getfirst() << endl;
	list1.print();
	cout << endl;
	list1.insert(12);
	list1.insert(12);
	list1.insert(12);
	list1.insert(15);
	list1.print();
	cout <<endl<< list1.getfirst() << endl;
	list1.remove_all(12);
	list1.print();
	cout << endl;
	
    return 0;
}

